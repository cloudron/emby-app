[1.0.0]
* Initial version

[1.1.0]
* Update to Emby version 4.2.0.40

[1.2.0]
* Update to Emby version 4.3.0.0

[1.2.1]
* Update to Emby version 4.3.0.1

[1.2.2]
* Update to Emby version 4.3.0.10

[1.2.3]
* Update to Emby version 4.3.0.12

[1.2.4]
* Update to Emby version 4.3.0.14

[1.2.5]
* Update to Emby version 4.3.0.16

[1.2.6]
* Update to Emby version 4.3.0.19

[1.2.7]
* Update to Emby version 4.3.0.20

[1.2.8]
* Update to Emby version 4.3.0.21

[1.2.9]
* Update to Emby version 4.3.0.22

[1.2.10]
* Update to Emby version 4.3.0.30

[1.3.0]
* Update to Emby version 4.3.1.0
* Fix stopping live streams killing recordings
* Fix m3u tuners with m3u8 sources
* Fix photo downloads
* Fix missing PlaybackPositionTicks in PlaybackStart event - Trakt
* Fix changing audio tracks when remote controlling another app
* Add black theme option
* Improve light theme

[1.4.0]
* Update to Emby version 4.4.0.40

[1.4.1]
* Update to Emby version 4.4.1.0

[1.4.2]
* Update to Emby version 4.4.2.0

[1.5.0]
* Use base image 2.0.0

[1.5.1]
* Update to Emby version 4.4.3.0

[1.6.0]
* Update screenshots and forum url

[1.7.0]
* Add VAAPI support (hardware transcoding)

[1.8.0]
* Update to Emby version 4.5.1.0
* Fix include by tags feature returning incorrect content in certain situations
* Fix SSL http2 errors on Windows 8 & 2012
* Fix connection issues when used with HAProxy
* Fix local network address detection causing virtual adapters to be preferred
* Don't try to schedule wakes when running as windows service
* Fix audio podcast playback on mono-based platforms
* Update MovieDb plugin to 1.3.7

[1.8.1]
* Update to Emby version 4.5.3.0
* Support clicking individual text items in list items
* Support add to play queue with drag and drop
* Support add to playlists and collections with drag and drop
* Add profile checking for MP2Video hardware decoders
* Add Nvidia NVDEC decoder variant for HEVC
* VAAPI: Use color formats from detection
* Prevent ffmpeg hw processing of 10bit when device doesn't support it
* Fix play all/shuffle on collections comprised of folders
* Various fixes for codec level detection
* Improve detail screen backdrop display
* Reduce messaging from server to android and iOS apps causing the app background processes to run
* Fix interlaced video being incorrectly stream copied with HLS
* Fix remote play of playlists being sorted by name
* Show songs on artist detail screen
* Support downloading subtitles during video playback
* Fix webp image support not being used in certain cases
* Update same time recording threshold to 15 minutes
* Fix repeated subtitle downloads of .sub format
* Add http version to server log
* Improve subtitle track selection when always show subtitles is selected
* Expand artist split whitelist
* Fix undefined dlna display name
* Improve ability of other devices to discover emby server
* New search interface for mobile and desktop
* Add top results section to search
* Improve searching for songs using album name
* Fix channel logos intermittently disappearing while scrolling the guide in Firefox
* Fix incorrect iOS scroll after cancelling context menu
* Fix artists added to collections not showing

[1.8.2]
* Update to Emby 4.5.4.0
* Fix regression in 4.5.3 causing slow browsing and search performance in certain situations
* Fix regression in 4.5.2 causing incorrect items to be queued to conversion tasks
* Fix Nvidia HEVC level detection

[1.8.3]
* Use new base image v3

[1.8.4]
* Update to Emby 4.6.1.0
* [changelog blog](https://emby.media/emby-server-46-released.html)

[1.8.5]
* Update Emby to 4.6.3.0
* Fix web app playback regressions of videos that successfully played in previous server version
* Fix sporadic cases of audio playback stopping in the web app
* Fix sporadic cases of subtitle burn in not working
* Update sort by date last episode added to not be affected by virtual items in the database
* Fix album items being added to playlist alphabetically
* Fix transcoding audio channels for converted files using the convert media feature

[1.8.6]
* Update Emby to 4.6.4.0
* Resolve cause of sporadic database locked errors
* Fix graphic subs burn-in with tone mapping on qsv

[1.8.7]
* Update Emby to 4.6.7.0
* Fix regression with local network multiple subnet detection
* Improve cleanup of transcoding processes
* Fix outgoing SSL errors on some Linux distros
* Fix android server database errors on deletion
* Fix android server ffmpeg errors on android 11
* Fix android server SSL errors with fanart.tv
* Fix QuickSync HW overlay with tone mapping
* Fixes for Apple TV top shelf content
* Fix cbr books sending wrong mime type
* Fix transcoding throttling not occurring with Chromecast
* Fix classic image series extraction
* Add south African ratings
* Improve camera upload error handling
* Fix music album parsing error when album name is ()
* Fix folder images getting replaced when not saving images in media folders
* Fix identify feature with music videos
* Fix episodes with season numbers greater than 200
* Improve efficiency of real-time monitor
* Improve DSD audio support over Dlna
* Fix scaling of wrong-sized graphical subtitles
* Fix incorrect hw context indication for D3D11VA decoders
* Allow for higher audio stream copy bitrate
* Support subs and subtitles folder case-insensitive
* Update album/song links with Included In section
* Set subtitle filename when downloading subtitle files to the browser
* Add Italian ratings
* Always record episodes when the option to check existing library is not selected
* Adjust recording image save behavior for series episodes that are sports
* Don't convert really large images to webp
* Support audio description and studio from copyright tag
* Support wm/year and original year tags
* Improve same subnet detection
* Add paths to log file for reference
* Allow deleting albums and artists
* Avoid resetting real-time monitor to parent folder when possible
* Improve series grouping in mixed content
* Remove rejection of large camera uploads to allow any file size
* Support .mid audio files
* Support sort_with embedded sort title for videos
* Prevent split button on missing episode detail screen
* Fix artists inheriting genres from albums when they shouldn't
* Fix detection of video stream vs. embedded image

[1.9.0]
* Update manifest to v2

[1.9.1]
* Update to base image v3.2

[1.10.0]
* Update Emby to 4.7.0.60
* [Full changelog](https://github.com/MediaBrowser/Emby.Releases/releases/tag/4.7.0.60)

[1.11.0]
* Update Emby to 4.7.1.0
* [Full changelog](https://github.com/MediaBrowser/Emby.Releases/releases/tag/4.7.1.0)
* Improve Chinese language detection of embedded audio and subtitle tracks
* Remove errant play/shuffle buttons appearing on various list screens in server web interface
* Display timestamps with lyrics

[1.12.0]
* Update Emby to 4.7.3.0
* [Full changelog](https://github.com/MediaBrowser/Emby.Releases/releases/tag/4.7.3.0)
* Add Episode Intro Detection and Skipping feature
* Add new table view option
* Add more fields for selection in poster views
* Add new theme options
* Fixes for downloads automatically redownloading
* Remember subtitle selections even when user option is set to None
* Fix graphic subtitle position when burning in subtitles with transcoding
* Fix sporadic cases of errors when reordering live tv channels
* Improve automatic channel mapping with xml tv
* Support sort by composer

[1.12.1]
* Update Emby to 4.7.4.0
* [Full changelog](https://github.com/MediaBrowser/Emby.Releases/releases/tag/4.7.4.0)

[1.12.2]
* Update Emby to 4.7.5.0
* [Full changelog](https://github.com/MediaBrowser/Emby.Releases/releases/tag/4.7.5.0)
* Fix a/v sync regressions with live tv and recordings
* Fix web app subtitle sizes being too small
* Various fixes with new table view feature

[1.12.3]
* Update Emby to 4.7.6.0
* [Full changelog](https://github.com/MediaBrowser/Emby.Releases/releases/tag/4.7.6.0)
* Fix regressions related to image extraction
* Improve the use of internet metadata with recordings
* Improve photo loading performance
* Include activity log database file when vacuuming databases
* Mark all video versions as played when toggling played
* Support shift to multi-select
* Change wording of transcoding reasons to be more user friendly
* Improve filter menu indicator
* Add Select None to multi-select lists
* Fix sort by sort title being case sensitive in some views
* Fixes for file names with years at beginning
* Improve dsd/dsf playback over Dlna

[1.12.4]
* Update Emby to 4.7.8.0
* [Full changelog](https://github.com/MediaBrowser/Emby.Releases/releases/tag/4.7.8.0)
* Various database performance improvements
* Stop scanning at the top of a library when it's unreachable
* DVR: Don't use episode title for comparison when it matches the series title
* DVR: Support keyword-based recordings
* Improved RTL supported in the web interface
* Add lock screen function to video player for mobile devices
* Support three way navigation menu toggle, to switch between full, mini and flyout modes
* Add more fields for selection in poster views
* Add image size display option
* Fix support for simultaneous music and photo playback
* Add option to control display of rating information at the start of video playback
* Change wording of transcoding reasons to be more user friendly
* Improve filter menu indicator
* Fix regression on artists list screen

[1.12.5]
* Update Emby to 4.7.9.0
* [Full changelog](https://github.com/MediaBrowser/Emby.Releases/releases/tag/4.7.9.0)
* Support copying settings from an existing user when creating a new one
* Improve library scan handling of unavailable folders
* Fix regressions related to user device restriction management
* Add several new webhooks, including new media added
* Support sorting playlists
* Improved right to left (RTL) support
* Update default subtitle font

[1.12.6]
* Update Emby to 4.7.10.0
* [Full changelog](https://github.com/MediaBrowser/Emby.Releases/releases/tag/4.7.10.0)
* Fix HeaderLiveTV missing translation text
* Delay triggering new item additions under after metadata download. This should help improve the experience with Trakt, Webhooks, notifications, etc.
* Fixed intermittent issue of recordings being stopped when series keep limit is set to one

[1.12.7]
* Update Emby to 4.7.11.0
* Update Cloudron base image to 4.0.0
* [Full changelog](https://github.com/MediaBrowser/Emby.Releases/releases/tag/4.7.11.0)
* Fix intermittent scroll problem with Live TV Guide
* Improve intro detection error handling

[1.12.8]
* Update Emby to 4.7.12.0
* Fix 172.X addresses always being considered private
* Don't allow local network addresses to be specified in x-forwarded-for and x-real-ip
* Adjust web app html tags to avoid false detection from Chrome as impersonating the Emby domain

[1.12.9]
* Update Emby to 4.7.13.0
* [Full changelog](https://github.com/MediaBrowser/Emby.Releases/releases/tag/4.7.13.0)
* Improve artist splitting for artists that have a / in their title
* Handle invalid data when parsing video rotation information
* Fix error with conversion feature and videos that have audio streams with 0 channels
* Various intro detection bug fixes
* Fix errors with SMB access on Linux when non-default server port is used
* Various transcoding related fixes
* Increase default server database cache size for new installs

[1.12.10]
* Update Emby to 4.7.14.0
* [Full changelog](https://github.com/MediaBrowser/Emby.Releases/releases/tag/4.7.14.0)
* Web app compatibility updates for iOS 17
* Improve playback of transcoded video in the Emby web app

[1.13.0]
* Update Emby to 4.8.0.80
* [Full changelog](https://github.com/MediaBrowser/Emby.Releases/releases/tag/4.8.0.80)

[1.13.1]
* Update Emby to 4.8.1.0
* [Full changelog](https://github.com/MediaBrowser/Emby.Releases/releases/tag/4.8.1.0)
* Start running hardware detection earlier on startup to try and avoid playback errors
* Portuguese tv naming fixes
* Support ja-jp language code
* Fix CA-A parental rating value
* Fix more like this not hiding played media when the option is set
* Fix sporadic cases of live streams ending early
* Fix multiple live tv views showing
* Fix manage collaboration showing for playlists when permission to share personal content is denied
* Various content restriction fixes

[1.13.2]
* Update Emby to 4.8.3.0
* [Full changelog](https://github.com/MediaBrowser/Emby.Releases/releases/tag/4.8.3.0)
* Nfo Metadata - Support multiple episodedetails nodes
* M3U Tuner - Support restricting imported channels based on group
* M3U Tuner - Support embedded xml guide data
* Fix 3D flag being lost in some cases on initial import
* Fix series scanning regression related to year in folder name
* Improve photo scanning error handling
* Add workaround for older Dlna devices to fix playback failures
* Fix temp files from database backup process not being cleaned up
* Fix playlist library folder not generating an image
* Start running hardware detection earlier on startup to try and avoid playback errors
* Fix playback errors when transcoding on older LG TV's
* Redesign music genre screen

[1.13.3]
* Update Emby to 4.8.4.0
* [Full changelog](https://github.com/MediaBrowser/Emby.Releases/releases/tag/4.8.4.0)
* Fixes related to scanning multi-part videos
* Improve missing episode error handling
* Add collections library
* Fix duplicate live tv views
* Don't generate images for playlist libraries
* Fix some subtitles being downloaded repeatedly
* Fix video rotation value not being scanned
* Fix library option for adult metadata not being used with Identify feature
* Fix genres not showing in music videos library
* Fix extras getting included when playing multiple items via remote control
* Add option to group notifications
* Fix person nfo not being saved when enabled
* Improve camera upload error handling
* Improve ass/ssa support with conversion feature
* Freshen up web app fonts and icons
* Group collection items by media type
* Support drag and drop with transcoding codec settings
* Add series recording option to check all libraries for existing episodes

[1.13.4]
* Update Emby to 4.8.5.0
* [Full changelog](https://github.com/MediaBrowser/Emby.Releases/releases/tag/4.8.5.0)
* Fix OpenCL regression affecting some Linux platforms

[1.13.5]
* Update Emby to 4.8.6.0
* [Full changelog](https://github.com/MediaBrowser/Emby.Releases/releases/tag/4.8.6.0)
* Improve log anonymization
* Extract video images at full resolution
* Fix incorrect items being played with play all or shuffle on a genre
* Fix incorrect http response code on failed login (should be 401 rather than 403)
* Support drag and drop reordering in home screen settings library order
* Support drag and drop reordering in the metadata editor for genres, tags, people and studios

[1.13.6]
* Update Emby to 4.8.7.0
* [Full changelog](https://github.com/MediaBrowser/Emby.Releases/releases/tag/4.8.7.0)
* Add server shutdown warning when there are active playback sessions or active recordings
* PGS subtitle position fixes when transcoding
* Convert subtitle offset dialog to a slider
* Fix unable to lock People
* Add Up Next to video OSD
* Search all subtitle providers on manual subtitle downloads
* Fix collections getting lost after subtitle downloading when no nfo file is present
* Various content visibility fixes for newly created libraries
* Improve log anonymization
* Support drag and drop reordering in all library settings
* Add playback speed directly to video player OSD

[1.14.0]
* Update Emby to 4.8.9.0
* [Full changelog](https://github.com/MediaBrowser/Emby.Releases/releases/tag/4.8.9.0)
* Improve camera upload error handling
* Fix libvips image conversion with smb paths
* Fix automatic reconnect of live tv m3u
* Fixes related to matching recordings with episode files already in the library
* Improve multi-version default selection
* Improve remote image content-type detection
* Fix database error when removing libraries with overlapping paths with other libraries
* Support setting shuffle state in the audio player
* Add Group By option on collection screens to choose between grouping collection items by media type or displaying all items in a single list
* Fix import collection options not showing in Emby library setup
* Fix error after adding a new person using the metadata editor
* Add prompt for playback when a channel is recording
* Support incremental seek speed using arrow keys
* Show more rows in video OSD guide
* Support launching videos from photo slideshow
* Auto-scroll the guide to the last played channel
* Improve log anonymization
* Transcoding fixes for audio only strm files
* Increase image extraction timeout
* Factor in disc number to audio image uniqueness when extracting images from audio files

[1.14.1]
* Update Emby to 4.8.10.0
* [Full changelog](https://github.com/MediaBrowser/Emby.Releases/releases/tag/4.8.10.0)
* Fix hardware transcoding regression on some Linux platforms

[1.15.0]
* Update Cloudron base image to 4.2.0

[1.16.0]
* checklist added to manifest

[1.17.0]
* Update base image to 5.0.0

