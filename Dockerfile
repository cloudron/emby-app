FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

RUN mkdir -p /app/code
WORKDIR /app/code

RUN apt-get update && apt-get -y install vainfo libva2 i965-va-driver && rm -r /var/cache/apt /var/lib/apt/lists

# renovate: datasource=github-releases depName=MediaBrowser/Emby.Releases versioning=semver
ARG EMBY_VERSION=4.8.9.0

# the postinst script errors without a running systemd https://stackoverflow.com/a/47852901
RUN wget https://github.com/MediaBrowser/Emby.Releases/releases/download/${EMBY_VERSION}/emby-server-deb_${EMBY_VERSION}_amd64.deb && \
    dpkg --unpack emby-server-deb_${EMBY_VERSION}_amd64.deb && \
    rm -f /var/lib/dpkg/info/emby-server.postinst && \
    dpkg --configure emby-server && \
    rm emby-server-deb_${EMBY_VERSION}_amd64.deb

RUN rm -rf /var/lib/emby/ && ln -s /app/data/emby /var/lib/emby

COPY start.sh /app/code/

CMD [ "/app/code/start.sh" ]
