#!/usr/bin/env node

'use strict';

/* global it, xit, describe, before, after, afterEach */

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    const ADMIN_USERNAME='admin';
    const ADMIN_PASSWORD='changeme';
    const BBB_LINK='http://distribution.bbb3d.renderfarming.net/video/mp4/bbb_sunflower_1080p_30fps_normal.mp4';
    // const BBB_LOCATION=path.resolve('bbb_sunflower_1080p_30fps_normal.mp4');
    const LOCATION = process.env.LOCATION || 'test';
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 10000;
    const MOVIES_FOLDER = '/app/data/libraries/movies';

    var browser;
    var app;

    before(async function () {
        await startBrowser();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function clearCache() {
        await browser.manage().deleteAllCookies();
        await browser.quit();
        await startBrowser();
    }

    async function startBrowser() {
        browser = null;
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        chromeOptions.addArguments(`--user-data-dir=${await fs.promises.mkdtemp('/tmp/test-')}`); // --profile-directory=Default
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
    }

    async function findElement(elem) {
        const es = await browser.findElements(elem);
        for (const e of es) {
            if (await e.isDisplayed()) return e;
        }
        throw new Error('Cannot find any visible element');
    }

    function waitForElement(elem) {
        return browser.wait(until.elementLocated(elem), TEST_TIMEOUT).then(function () {
            return browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
        });
    }

    async function waitForElementAsync(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function classXpath(className) {
        return `//*[contains(concat(' ',normalize-space(@class), ' '),' ${className} ')]`;
    }

    async function setupWizard() {
        await browser.sleep(10000);

        await browser.manage().deleteAllCookies();
        await browser.get('https://' + app.fqdn);

        await waitForElementAsync(By.xpath(classXpath('selectLocalizationLanguage')));
        await browser.executeScript('document.getElementsByClassName("selectLocalizationLanguage")[0].value = "en-US"');
        await browser.sleep(2000);
        await browser.findElement(By.xpath('//button[@type="submit"]')).click();

        await waitForElementAsync(By.xpath(classXpath('txtUsername')));
        await browser.findElement(By.xpath(classXpath('txtUsername'))).clear();
        await browser.findElement(By.xpath(classXpath('txtUsername'))).sendKeys(ADMIN_USERNAME);
        await browser.findElement(By.xpath(classXpath('txtPassword'))).clear();
        await browser.findElement(By.xpath(classXpath('txtPassword'))).sendKeys(ADMIN_PASSWORD);
        await browser.findElement(By.xpath(classXpath('txtPasswordConfirm'))).clear();
        await browser.findElement(By.xpath(classXpath('txtPasswordConfirm'))).sendKeys(ADMIN_PASSWORD);
        await browser.sleep(5000);
        (await findElement(By.xpath('//button[contains(., "Next")]'))).click();
        await browser.sleep(5000);

        await waitForElementAsync(By.xpath(classXpath('btnNewLibrary')));
        await browser.findElement(By.xpath(classXpath('btnNewLibrary'))).click();
        await waitForElementAsync(By.xpath(classXpath('selectCollectionType')));
        await browser.executeScript('document.getElementsByClassName("selectCollectionType")[0].selectedIndex = 1');
        await browser.findElement(By.xpath(classXpath('btnAddFolder'))).click();
        await waitForElementAsync(By.xpath(classXpath('txtDirectoryPickerPath')));
        await browser.sleep(5000);
        await browser.findElement(By.xpath(classXpath('txtDirectoryPickerPath'))).sendKeys(MOVIES_FOLDER);
        await browser.sleep(5000);
        await browser.findElement(By.xpath(classXpath('txtDirectoryPickerPath'))).sendKeys(Key.RETURN);
        await browser.sleep(5000);
        await browser.findElement(By.xpath('//input[@label="Display name"]')).sendKeys('Movies');
        await browser.sleep(5000);
        await waitForElementAsync(By.xpath('//button[.//span[contains(text(), "OK")]]'));
        await browser.findElement(By.xpath('//button[.//span[contains(text(), "OK")]]')).click();
        await browser.sleep(5000);
        await waitForElementAsync(By.xpath('//button[contains(@class, "btnWizardNext")]'));
        await browser.findElement(By.xpath('//button[contains(@class, "btnWizardNext")]')).click();

        await browser.sleep(5000);
        await waitForElementAsync(By.xpath('(//button[contains(.,"Next")])[1]'));
        await browser.findElement(By.xpath('(//button[contains(.,"Next")])[1]')).click();

        await waitForElementAsync(By.xpath('//label[./input[contains(@class, "chkAccept")]]//span'));
        await browser.findElement(By.xpath('//label[./input[contains(@class, "chkAccept")]]//span')).click();
        await browser.sleep(5000);
        await browser.findElement(By.xpath('(//button[contains(.,"Next")])[2]')).click();
        await waitForElementAsync(By.xpath('//button[.//span[contains(text(), "Finish")]]'));
        await browser.findElement(By.xpath('//button[.//span[contains(text(), "Finish")]]')).click();
        await waitForElementAsync(By.xpath('//h1[contains(text(), "Sign in to")]'));
    }

    async function login(username, password) {
        await browser.manage().deleteAllCookies();
        await browser.get('https://' + app.fqdn);
        await waitForElement(By.xpath(classXpath('txtUserName')));
        await browser.findElement(By.xpath(classXpath('txtUserName'))).sendKeys(username);
        await browser.findElement(By.xpath(classXpath('txtPassword'))).sendKeys(password);
        await browser.findElement(By.xpath('//button[@type="submit"]')).click();
        await waitForElement(By.xpath('//h2[contains(text(), "My Media")]'));
    }

    async function logout() {
        await clearCache();
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    }

    function contentAdd(callback) {
        console.log('Downloading test video into app')
	    execSync(`cloudron exec --app ${LOCATION} -- wget -ncv ${BBB_LINK} -O ${MOVIES_FOLDER}/BBB.mp4`, EXEC_ARGS);

        setTimeout(callback, 60000);
    }

    async function contentExists() {
        await browser.get('https://' + app.fqdn);
        waitForElementAsync(By.xpath("//button[@title='BBB']"));
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('setup wizard', setupWizard);
    it('can login as admin', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));

    it('can add content', contentAdd);
    it('content exists', contentExists);
    it('can logout', logout);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });

    it('can login as admin', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('content exists', contentExists);
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', function () { execSync(`cloudron restore --app ${app.id}`, EXEC_ARGS); });

    it('can login as admin', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('content exists', contentExists);
    it('can logout', logout);

    it('move to different location', function (done) {
        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
            done();
        });
    });

    it('can get app information', getAppInfo);
    it('can login as admin', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('content exists', contentExists);
    it('can logout', logout);

    it('uninstall app', function (done) {
        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
            done();
        });
    });

    // test update
    it('install app', function () { execSync(`cloudron install --appstore-id media.emby.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('setup wizard', setupWizard);
    it('can login as admin', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('can add content', contentAdd);
    it('content exists', contentExists);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);

    it('content exists', contentExists);

    it('uninstall app', function (done) {
        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
            done();
        });
    });
});
