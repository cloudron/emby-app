### Overview

Emby Server is a personal media server with apps on just about every device.

Bringing all of your home videos, music, and photos together into one place has
never been easier. Your personal Emby Server automatically converts and streams
your media on-the-fly to play on any device.

### Features
* Your Media on Any Device
* Easy Access
* Live TV
* Mobile Sync
* Manage Your Home
* Chromecast
* Beautiful Displays
* Parental Controls
* Cloud Sync
* Manage Your Media
* Easy DLNA
* Web-Based Management
