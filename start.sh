#!/bin/bash

set -eu

echo "=> Ensure directories"
if [[ ! -d "/app/data/emby" ]]; then
    mkdir -p /app/data/emby

    # create some convenience folders
    mkdir -p /app/data/libraries/movies/ /app/data/libraries/shows/ /app/data/libraries/photos/ /app/data/libraries/music/
fi

echo "=> Ensure permissions"
chown -R cloudron:cloudron /app/data

echo "=> Start emby-server"
exec /usr/local/bin/gosu cloudron:video /opt/emby-server/bin/emby-server
