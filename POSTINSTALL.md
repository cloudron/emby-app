This package is setup to have libraries in the following directories:

* Movies - `/app/data/libraries/movies`
* Shows - `/app/data/libraries/shows`
* Photos - `/app/data/libraries/photos`
* Music - `/app/data/libraries/music`

Content has to be uploaded into Emby using SFTP or the File Manager.

